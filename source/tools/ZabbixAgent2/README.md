# Zabbix-agent2 installation and configuration

> How to install and configure zabbix agent on host

## Installation
Prepare installation according doc: (prepare registry and update)
[zabbix-installation](https://www.zabbix.com/download?zabbix=6.0&os_distribution=ubuntu&os_version=20.04_focal&db=mysql&ws=apache)

```shell
# install agent and disable default service
sudo apt-get install -y zabbix-agent2
sudo systemctl disable zabbix-agent2.service
```

## Configuration
Allow outgoing connection from agent host
```shell
sudo ufw allow out 33051 && sudo ufw reload

# get IP of agent machine
ifconfig
```

Allow incomming connection on **Zabbix server** host port for agent IP
```shell
sudo ufw allow from <AGENT-IP> to any port 33051 && sudo ufw reload
```

Create zabbix agent config directory
```shell
mkdir /var/oidis/apps/zabbix-agent2

cd /var/oidis/apps/zabbix-agent2
```

Open zabbix_agetn2.conf a .service file too to update real paths and agent hostname.

Generate PSK key file in agent folder.
```shell
openssl rand -hex 64 > /var/oidis/apps/zabbix-agent2/zabbix_agent2.psk
```

Copy service configuration into search path (with modified file name).
```shell
sudo cp zabbix_agent2.service /etc/systemd/system/zabbix_agent2_oidis.service
```

Start service.
```shell
sudo systemctl enable zabbix_agent2_oidis.service
sudo systemctl start zabbix_agent2_oidis.service
```

## Zabbix web UI configuration
Configure Host on Zabbix server:
- Log in as admin or maintainer account
- Configuration->Hosts
- Create host for demand machine if not exist or modify
  - Open encryption tab
  - Enable PSK and disable no-encryption for "Connection from host"
  - Enter PSK identity (specified in zabbix_agent2.conf) and PSK key from generated .psk file
  - It takes several minutes before new data will be available in "Monitoring->Latest data"

## Troubleshooting
- Zabbix server
```shell
docker logs zabbix-server
```

- Zabbix agent
```shell
# real log file path is configured in zabbix_agent2.conf
cat /var/log/zabbix/zabbix_agent2_oidis.log
```

---

Author Michal Kelnar,
Copyright 2022 [Oidis](https://oidis.org/)
