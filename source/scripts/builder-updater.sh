#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *
hubUrl="${HUB_LOCATION:-hub.eap.oidis.io}"
builderType="${BUILDER_TYPE:-shared}"
builderConfName=WuiBuilder.config.jsonp
confPath="${CONFIG_PATH:-/var/oidis/docker/WuiBuilder.config.jsonp}"
wuiLinkPath="/usr/bin/wui"
builderSrc="${BUILDER_SRC:-io-oidis-builder}"

unameOut="$(uname -s)"
case "${unameOut}" in
Linux*)
  builderOs="${BUILDER_OS:-linux}"
  echo "Detected platform: Linux"
  ;;
Darwin*)
  builderOs="${BUILDER_OS:-mac}"
  wuiLinkPath="/usr/local/bin/wui"
  echo "Detected platform: Linux"
  ;;
*)
  echo "Wrong machine detected" 1>&2
  exit 1
  ;;
esac

for i in "$@"; do
  case $i in
  -h | --help)
    echo "Builder updater."
    echo "  --help              Prints help."
    echo "  --builder-src=*     Builder source path (destination)."
    echo "  --import-cfg=*      Import private config from path."
    exit 0
    ;;
  --builder-src=*)
    builderSrc="${i#*=}"
    shift
    ;;
  --import-cfg=*)
    confPath="${i#*=}"
    shift
    ;;
  -*)
    echo "Unknown option $i" 1>&2
    exit 1
    ;;
  *) ;;
  esac
done

echo "Running with:"
echo "  builderOs:   $builderOs"
echo "  builderSrc:  $builderSrc"
echo "  hubUrl:      $hubUrl"
echo "  builderType: $builderType"
echo "  confPath:    $confPath"
echo " --- "

if [[ -z "${builderOs}" || -z "${builderSrc}" ]]; then
  echo "Builder OS and source path needs to be specified" 1>&2
  exit 1
fi
if [[ -z "${hubUrl}" || -z "${builderType}" ]]; then
  echo "Hub location and builder platform type needs to be defined" 1>&2
  exit 1
fi

builderSrc=$(realpath "$builderSrc")

if [[ -f $builderSrc/$builderConfName ]]; then
  echo "Reading private config data"
  confData=$(cat "$builderSrc/$builderConfName")
  cp "$builderSrc"/$builderConfName /tmp/$builderConfName
  echo "Private config backup: /tmp/$builderConfName"
fi

if ! [[ -d $builderSrc ]]; then
  mkdir -p "$builderSrc"
fi

cd "$builderSrc" || exit

rm -rf ./*

echo "Downloading builder..."
curl -fsSL --compressed "https://$hubUrl/Update/io-oidis-builder/null/$builderType-$builderOs-nodejs" >builder.tar.gz
echo "Unpacking builder..."
tar xf builder.tar.gz
cd io-oidis-builder-* || exit
cp -r . ../
cd ..
rm -rf io-oidis-builder-*
rm builder.tar.gz
cd ..

echo "Builder prepared"
if [ -n "$confData" ]; then
  echo "$confData" >"$builderSrc/$builderConfName"
  echo "Loading private config from backup"
else
  confPathReal=$(realpath --quiet "$confPath")
  if [[ -f $confPathReal ]]; then
    cp -P "$confPathReal" "$builderSrc/$builderConfName"
    echo "Importing private config from external path"
  else
    echo "Import of private config '$confPath' (realpath: '$confPathReal') skipped: source file not found"
  fi
fi

function createLink() {
  echo "Creating builder link"
  sudo ln -sfn "$builderSrc"/cmd/wui.sh $wuiLinkPath || exit 1
}

if [ -L "$(which wui)" ]; then
  echo "Builder symlink already exists"
  linkPath=$(readlink "$(which wui)")
  if [ "$linkPath" = "$builderSrc/cmd/wui.sh" ]; then
    echo "wui - symlink targets proper file path"
  else
    echo "wui - symlink targets different file path"
    while true; do
      read -p "Do you want to overwrite 'wui' symlink? (y/N): " -r yn
      case $yn in
      [yY])
        createLink
        break
        ;;
      *)
        echo "Symlink overwrite skipped by user choice"
        break
        ;;
      esac
    done
  fi
else
  createLink
fi

echo "Builder updated"
