#!/bin/bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *
application="backup"
deploy="nexus"
dbHost="127.0.0.1"

for i in "$@"; do
  case $i in
  -h | --help)
    echo "This is generic DB backup tool which is able to dump MongoDB and MySQL databases and deploy the dump to nexus or mounted disk."
    echo "  --help          Prints help."
    echo "  --profile=*     Specify one of dev|eap|prod profile."
    echo "  --type=*        Select one of supported database mongo|mysql."
    echo "  --app=*         Specify application name or namespace."
    echo "  --db-name=*     Database table/document name to backup."
    echo "  --db-host=*     Database host."
    echo "  --db-port=*     Database port."
    echo "  --db-user=*     Database user with permissions to specified DB table/document."
    echo "  --db-pass=*     Database user password with permissions to specified DB table/document."
    echo "  --db-auth=*     Select authentication database (mongo only)."
    echo "  --deploy=*      Select one of supported deploy type nexus|dir."
    echo "  --path=*        Specify path to deploy (relevant for dir deploy type right now)."
    exit 0
    ;;
  --profile=*)
    profile="${i#*=}"
    shift
    ;;
  --type=*)
    type="${i#*=}"
    shift
    ;;
  --app=*)
    application="${i#*=}"
    shift
    ;;
  --db-name=*)
    dbName="${i#*=}"
    shift
    ;;
  --db-host=*)
    dbHost="${i#*=}"
    shift
    ;;
  --db-port=*)
    dbPort="${i#*=}"
    shift
    ;;
  --db-user=*)
    dbUser="${i#*=}"
    shift
    ;;
  --db-pass=*)
    dbPass="${i#*=}"
    shift
    ;;
  --db-auth=*)
    dbAuth="${i#*=}"
    shift
    ;;
  --deploy=*)
    deploy="${i#*=}"
    shift
    ;;
  --path=*)
    deploy="dir"
    deployPath="${i#*=}"
    shift
    ;;
  -*)
    echo "Unknown option $i" 1>&2
    exit 1
    ;;
  *) ;;
  esac
done

if [[ $type != "mongo" && $type != "mysql" ]]; then
  echo "DB type needs to be specified, please add --type=[mongo|mysql] and run again" >>/dev/stderr
  exit 1
fi

if [[ $deploy != "nexus" && $deploy != "dir" ]]; then
  echo "DB type needs to be specified, please add --type=[nexus|dir] and run again" >>/dev/stderr
  exit 1
fi

if [[ -z ${dbName} ]]; then
  echo "Database name (--db-name=<name>) needs to be specified" >>/dev/stderr
  exit 1
fi

if [[ $type == "mongo" ]]; then
  dumpName="${application}Dump_$(date '+%Y%m%d%H%M%S').tar.gz"
  dumpPath="/tmp/$dumpName"

  if [[ -z ${dbPort} ]]; then
    dbPort=27017
  fi
  args=()
  if [[ -n "$dbUser" ]]; then
    if [[ -z "$dbPass" ]]; then
      echo "Missing password, please add --db-pass=<password>" >>/dev/stderr
      exit 1
    fi
    args=(--username="$dbUser" --password="$dbPass" --authenticationDatabase="$dbAuth")
  fi
  mongodump --host="$dbHost" --port="$dbPort" --db="$dbName" --out="${application}Dump" --forceTableScan "${args[@]}" || exit 1

  tar -czvf "$dumpPath" "${application}Dump" || exit 1
  rm -rf IoVitApplicationDump
else
  dumpName="${application}Dump_$(date '+%Y%m%d%H%M%S').sql"
  dumpPath="/tmp/$dumpName"

  if [[ -z ${dbPort} ]]; then
    dbPort=3306
  fi
  args=()
  if [[ -n "$dbUser" ]]; then
    if [[ -z "$dbPass" ]]; then
      echo "Missing password, please add --db-pass=<password>" >>/dev/stderr
      exit 1
    fi
    args=(--user="$dbUser" --password="$dbPass")
  fi
  mysqldump -alv --single-transaction --host="$dbHost" --port=$dbPort "${args[@]}" --result-file="$dumpPath" "$dbName" || exit 1
fi

# // TODO(mkelnar) add compression of mysql and investigate possibility to passphrase archive

if [[ $deploy == "nexus" ]]; then
  if [[ $profile != "dev" && $profile != "eap" && $profile != "prod" ]]; then
    echo "Profile needs to be specified for nexus deploy, please add --profile=[dev|eap|prod] and run again" >>/dev/stderr
    exit 1
  fi

  wui nexus:"$profile" --no-target --file="$dumpPath"
else
  output=$deployPath
  if [[ -d "$deployPath" ]]; then
    output="$deployPath/$dumpName"
  else
    output=$deployPath
  fi

  if [[ -f "$(dirname "$output")" ]]; then
    echo "Output directory not exists: $output" >>/dev/stderr
    exit 1
  fi

  echo "Copy archived dump to $output"
  cp "$dumpPath" "$output"
fi

rm "$dumpPath"
