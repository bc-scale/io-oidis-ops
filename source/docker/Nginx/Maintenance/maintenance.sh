#!/usr/bin/env bash
# * ********************************************************************************************************* *
# *
# * Copyright 2022 Oidis
# *
# * SPDX-License-Identifier: BSD-3-Clause
# * The BSD-3-Clause license for this file can be found in the LICENSE.txt file included with this distribution
# * or at https://spdx.org/licenses/BSD-3-Clause.html#licenseText
# *
# * ********************************************************************************************************* *

isMntOn=0
serviceNormalCfg="dev.nginx.conf"
serviceMntCfg="dev.mnt.conf"

function help() {
  echo "Switch proxy to maintanance mode and reload if swap is ."
  echo "Usage: {script-name} on|off [options]"
  echo "  -h|--help    Prints help"
  echo "  --with-swap  Swaps .nginx.conf and .mnt.conf profiles, on/off proxy static page othewise"
  echo "  on           Turn on maintenance mode for cwd relate app"
  echo "  off          Turn off maintenance mode for cwd relate app"
}

function argParse() {
  for i in "$@"; do
    case $i in
    -h | --help)
      help
      exit 0
      ;;
    on)
      isMntOn=1
      shift
      ;;
    off)
      isMntOn=2
      shift
      ;;
    --with-swap)
      isSwapOn=1
      shift
      ;;
    -* | *)
      logError "Unknown option $i"
      help
      exit 1
      ;;
    esac
  done
}

function mntOn() {
  echo "Maintenance: ON"
  touch mnton.txt

  if [[ $isSwapOn == 1 ]]; then
    if [[ -f "$serviceMntCfg.backup" ]]; then
      mv $serviceMntCfg.backup $serviceMntCfg
    elif [[ ! -f "$serviceMntCfg" ]]; then
      echo "Can not find maintenance proxy profile for current application" 2>&1
      exit 1
    fi

    if [[ -f "$serviceNormalCfg" ]]; then
      mv $serviceNormalCfg $serviceNormalCfg.backup
    fi

    proxy-reload
  fi
}

function mntOff() {
  echo "Maintenance: OFF"
  rm mnton.txt

  if [[ $isSwapOn == 1 ]]; then
    if [[ -f "$serviceNormalCfg.backup" ]]; then
      mv $serviceNormalCfg.backup $serviceNormalCfg
    elif [[ ! -f "$serviceNormalCfg" ]]; then
      echo "Can not find proxy profile for current application" 2>&1
      exit 1
    fi

    if [[ -f "$serviceMntCfg" ]]; then
      mv $serviceMntCfg $serviceMntCfg.backup
    fi

    proxy-reload
  fi
}

argParse "$@"

if [[ $isMntOn == 1 ]]; then
  mntOn
elif [[ $isMntOn == 2 ]]; then
  mntOff
else
  help
  exit 0
fi