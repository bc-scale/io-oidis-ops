# replace root nginx.conf in /etc/nginx/ folder by root.nginx.conf

sudo mv root.nginx.conf /etc/nginx/nginx.conf

# remove default cron updater - to be validated
rm /etc/cron.d/certbot

# create crontab entry for system user
0 12 * * * /var/oidis/main-proxy/cert-update.sh
