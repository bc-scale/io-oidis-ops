# io-oidis-hub - MongoDB

> How to setup MongoDB for Oidis Hub based applications

## Docker setup
Since we are starting with more ready to go setups for docker, please take into account that old approach for docker service creation 
is no longer recommended.
```shell
docker-compose up -d
docker-compose down
```
Is recommended to be replaced default cfg by explicit cfg specification during compose like below.
```shell
docker-compose -f <filename.yml> up -d
docker-compose -f <filename.yml> down
```
This has several advantages which lies in full configuration of service without commented parts and other options which are only in 
knowledge base of user. Yes, and it is also recommended to not use ".env" file anymore because it is not supported by swarm's composing
so override yml directly if needed (especially on server/production/..., local machine deployment could be used as usual for now).

---
**NOTE:** If you want to use old "docker-compose up -d" then just rename demand file or create symlink to one of supported filenames for
default compose search (printed by first run without -f or just use "docker-compose.yml").

---

### Presets
There are at least two prepared presets.
 - compose.local.yml - suitable for local development and experimentation, while forced authentication is disabled by default 
 - compose.prod.yml - with required authentication

## MongoDB Container setup
Start container for very first time with or without default root user creation.
Start container - attach terminal or command below

```shell
# find mongo container name
docker ps

# attach internal container terminal
docker exec -it <container-name> bash

# open mongo terminal (this could be used also instead of "bash" in command above)
mongo
```

Another option is to install [mongosh](https://docs.mongodb.com/mongodb-shell/install/#std-label-mdb-shell-install) 
on host machine.
```shell
mongosh --port <mongo-port>

# or with automatic login you can skip next step for db.auth()
mongosh --port <port> --username <username> --authenticationDatabase <admin>
```

**Rest of this manual is the same for both connection types mentioned before.**

If you have used default root user creation by mongo implicit first run init then you will need to 
log in into mongo db.
```shell
# switch into admin mode
use admin

# you can enter raw string password instead prompt function if needed
db.auth({user:"<username>", pwd:passwordPrompt()})
```

## Database users
According to Mongo documentation there should be at least one admin account while it is not necessary to have "root" role. This admin 
account should be located in main DB which is "admin" by default and this will be used also as **authSource** by default.
Create administration account according to [official manual](https://docs.mongodb.com/manual/tutorial/configure-scram-client-authentication).
Specify proper username and save password which will be entered by prompt in command line.
```
use admin
db.createUser(
    {
        user: "oidisroot",
        pwd: passwordPrompt(), // or plaintext password
        roles: [
            { role: "userAdminAnyDatabase", db: "admin" },
            { role: "readWriteAnyDatabase", db: "admin" }
        ]
    }
)
```
You can validate credentials by using db.auth() in mongo shell.
```
db.auth({user:"oidisdbroot", pwd:passwordPrompt()})
```

Previously created admin account should be used only for maintenance and data users management. So for any application scoped database 
should be accessed only by specific user or users with limited rights.
Create user account for single application DB only while selected DB has to match with "dbConfig" in application solution. Once this setup 
is done then specify user and pass back in the application "dbConfig".
```
db.createUser(
    {
        user: "oidishubuser",
        pwd: passwordPrompt(), // or plaintext password
        roles: [
            { role: "readWrite", db: "IoOidisHub" }
        ]
    }
)
```

Restart docker service to apply changes.

---

Author Michal Kelnar, 
Copyright 2021 [Oidis](https://oidis.org/)
