# io-oidis-hub v2022.2.0

> Repository for OPS stuff and helper tools for deployment

## License

This software is owned or controlled by Oidis.
The use of this software is governed by the BSD-3-Clause Licence distributed with this material.

See the `LICENSE.txt` file for more details.

---

Author Michal Kelnar,
Copyright 2022 [Oidis](https://www.oidis.org/)
